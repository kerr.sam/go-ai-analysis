
KATA_PATH="C:\\katago\\katago.exe"
ANALYSIS_CFG_PATH="C:\\katago\\analysis_example.cfg"
MODEL_PATH="C:\\katago\\g170e-b20c256x2-s5303129600-d1228401921.bin.gz"
SGF_PATH = "C:\\Users\\kerrs\\Code\\go_kata_scanner\\sgf_files"

DEVELOPMENT_MODE = True
DEBUG_MODE = False

if DEVELOPMENT_MODE == True:
    MAX_VISITS = 5 # Set to None to use defaults
    MAX_TIME = 5 # Set to None to use defaults
else:
    MAX_VISITS = None # Set to None to use defaults
    MAX_TIME = None # Set to None to use defaults

import os
import json
import subprocess
from sgfmill import sgf,common
from operator import itemgetter, attrgetter
import sys

#kata = None
#game = None

def utf8_string(game, name):
    return name.encode(game.get_root().get_encoding()).decode('utf-8')

def ascii_hash(string):
    import hashlib
    return hashlib.sha1(string.encode('utf-8')).hexdigest()

def get_handicap_stones(game):
    initialStones = []
    if game.get_handicap() != None:
        print("Handicap stones: %d" % game.get_handicap())
        print (game.get_root().get("AB"))

        for handicap_stone in game.get_root().get("AB"):
            initialStones.append(["B", common.format_vertex(handicap_stone)])

        print (initialStones)
        print("Added handicap stones at: %s" % initialStones)
    return initialStones

def start_kata_go():
    if MAX_VISITS != None or MAX_TIME != None:
        visit_string = "-override-config "
        if MAX_TIME:
            visit_string = visit_string + "maxSeconds=%d," % MAX_TIME
    else:
        visit_string = ""
    visit_string = visit_string.rstrip(',')

    command = ("%s analysis -config %s -model %s %s" % (KATA_PATH, ANALYSIS_CFG_PATH, MODEL_PATH, visit_string))
    print("[*] Starting KataGo with command: %s" % command)
    kata = subprocess.Popen(command.split(),
                            universal_newlines=True,
                            bufsize=1,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE
                            )

    import threading
    def monitor_kata(kata_pid):
        """ Check For the existence of a unix pid. """
        while True:
            import time, psutil
            time.sleep(15)

            if not psutil.pid_exists(kata_pid):
                sys.stdout.write("[X] KataGo died! Killing analysis\n")

                sys.exit(12)
            else:
                sys.stdout.write("[*] KataGo appears alive\n")



    print("[*] Starting Kata Watchdog thread. Kata on PID %d" % kata.pid)
    kata_watchdog = threading.Thread(target=monitor_kata, args=(kata.pid,), daemon=True)
    kata_watchdog.start()

    return kata

def send_to_kata(kata, kata_msg):
    print("[*] About to send to KataGo")
    if DEBUG_MODE:
        print(">>> %s" % kata_msg)
    if kata.stdin.writable() == False:
        print("[X] KataGo not writable!")
        return
    kata.stdin.write(kata_msg + "\n") # Make sure to send a newline to get Kata to act on the message

def process_kata_response(msg):
    print("[*] Processing KataGo output")
    print(">>> %s" % msg)

    analysis = json.loads(msg)
    #print(json.dumps(analysis, indent=2, sort_keys=True))

    analysis['moveInfos'].sort(key=lambda x:x['order'])
    our_analysis = {
        'winrate': analysis['rootInfo']['winrate'],
        'utility': analysis['rootInfo']['utility'],
        'visits': analysis['rootInfo']['visits'],
        'scoreLead': analysis['rootInfo']['scoreLead'],
        'turnNumber': analysis['turnNumber'],
        'bestKataMove': analysis['moveInfos'][0]['move'],
        'playedMove': None,
        'playedColor': None,
        'sgf': analysis['id']
        #'includePolicy':True # Adds 19x19 grid to output for policy
    }

    return our_analysis

def sanity_check_game(game):
    board_size = game.get_size()

    # Check to make sure we don't process games we don't know how to measure
    if board_size != 19:
        print("Only can process 19x19 games! Bad SGF at %s" % file)
        return None

    root_node = game.get_root()
    if root_node.has_property("HA") and root_node.get("HA") == 1:
        print("[!] Handicap is 1, assuming Fox and setting to 0.")
        root_node.set("HA", 0)

    if game.get_komi() > 100:
        print("[!] Komi > 100, assuming Fox and dividing by 100")
        root_node.set("KM", game.get_komi() / 100.0)

    if game.get_komi() % 0.5 != 0:
        print("[!] Komi not even or half point. Updating.")
        root_node.set("KM", int(game.get_komi()) + 0.5 if game.get_komi() % 100 > 0 else 0)
        print("[!] Komi is now %s" % root_node.get("KM"))

    if root_node.has_property('AP') and len(root_node.get_raw_list('AP')) > 1:
        print('[!] Multiple applications listed, attempting to manually update.')

        apps = game.get_root().find("AP").get_raw_list('AP')

        if b'foxwq' in apps:
            print('[!] Appears to be an SGF from Fox, updating AP and PC values')
            root_node.set('PC', 'Fox Go Server')
            root_node.unset('AP') # Fox hardcodes GNU Go for application

        else:

            new_apps = []
            for i in apps:
                if i == b'GNU Go:3.8':
                    continue
                new_apps.append(i)

            if len(new_apps) > 1:
                print('[!] Still have too many apps. Truncating to: %s' % new_apps[0])

            # The AP property is of the form "program:version number", so it must be a two element list
            # Reference the spec at https://www.red-bean.com/sgf/properties.html#AP
            new_apps = str(new_apps[0], 'utf-8')
            new_apps = new_apps.split(':')
            if len(new_apps) == 1:
                new_apps.append('')

            print('[!] New app after sanity check: %s' % (new_apps))
            root_node.set("AP", new_apps)

    return game

def load_game(file):
    fullpath = SGF_PATH + os.sep + file
    print("[*] Loading game %s" % fullpath)
    with open(fullpath,"rb") as f:
        game = sgf.Sgf_game.from_bytes(f.read())
    game = sanity_check_game(game)
    return game


def process_sgf(kata, file, game, gamename=None):
    board_size = game.get_size()

    komi = game.get_komi()
    root_node = game.get_root()

    kata_msg = {
        "id":"%s" % ascii_hash(gamename) if gamename != None else "foo", # Set the id to SGF name in case we analyze many games at once. Temp hash since may have unicode we need to convert later
        "initialStones":get_handicap_stones(game),
        "moves":[],
        "boardXSize": 19,
        "boardYSize": 19,
        "komi": komi,
        "rules": game.get_root().get('RU') # "japanese"
    }

    if MAX_VISITS != None:
        kata_msg['maxVisits'] = MAX_VISITS

    for node in game.get_main_sequence():
        next_move = node.get_move()
        if next_move[0] == None:
            continue

        if next_move[1] == None:
            # This is a pass
            kata_move = [str.upper(next_move[0]), "pass"]
        else:
            #kata_move = [str.upper(next_move[0]), "%s%s" % (chr(next_move[1][0]+ord('A')), next_move[1][1]+1)] # Add one because of 0-offset
            kata_move = [str.upper(next_move[0]), "(%s,%s)" % (next_move[1][0], next_move[1][1])]

        kata_move = [str.upper(next_move[0]), "%s" % common.format_vertex(next_move[1])] # Add one because of 0-offset

        kata_msg['moves'].append(kata_move)

    with open('kata_msg_%s.serialized' % file, 'w') as debugged:
        debugged.writelines(json.dumps(kata_msg))

    MOVES_TO_CHECK=len(kata_msg['moves']) + 1

    for i in range(MOVES_TO_CHECK):
        kata_msg['analyzeTurns'] = [i]
        send_to_kata(kata, json.dumps(kata_msg))
    return MOVES_TO_CHECK

def collect_kata_responses(kata, expected_moves):
    analysis=[]
    for line in iter(kata.stdout.readline, b''):
        next_line = line.rstrip()
        print('[*] Reading from KataGo')
        print(">>> " + line.rstrip())

        if line.rstrip() == '':
            continue
        analysis.append(process_kata_response(line))

        if len(analysis) == expected_moves:
            break
        else:
            print('[*] On move %d of %d' % (len(analysis), expected_moves))

    #analysis.sort(key=lambda x: x["turnNumber"])
    print (analysis)
    return analysis

def serialize_analysis(analysis):

    import csv, datetime
    with open('csv_analysis_%s.csv' % (datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")),'w', newline='', encoding='utf-8') as f:
        csv_writer = csv.writer(f)
        dict_writer = csv.DictWriter(f, analysis[0].keys())
        dict_writer.writeheader()
        dict_writer.writerows(analysis)

def get_move_index(analysis, sgf, move_number):
    for index, entry in enumerate(analysis):
        if entry['sgf'] == sgf and entry['turnNumber'] == move_number:
            return index
    return None

def get_game_length(analysis, sgf):
    filtered_analysis = list(filter(lambda x:x['sgf'] == sgf, analysis))
    return len(filtered_analysis)

def get_move(analysis, sgf, move_number):
    filtered_analysis = filter(lambda x:x['sgf'] == sgf, analysis)
    for entry in filtered_analysis:
        if entry['turnNumber'] == move_number:
            return entry
    print("[X] Couldn't find move %d in file %s" % (move_number, sgf))
    return None

def annotate_analysis(analysis, filepath):

    print("[*] Annotating for file %s" % filepath)
    game = load_game(filepath)

    player_names = {
        'b': utf8_string(game, game.get_player_name('b')),
        'w': utf8_string(game, game.get_player_name('w'))
    }
    print("[*] Players are: %s" % player_names)

    moves = []
    for node in game.get_main_sequence():
        next_move = node.get_move()
        print (next_move)
        if next_move[0] == None:

            pass

        moves.append(next_move)

    print("There are %d moves" % len(moves))
    for index, entry in enumerate(analysis):
        if not entry['sgf'] == os.path.basename(filepath):
            # Not our SGF, skip this entry
            continue

        # We have a move, look it up in the analysis now
        this_move = moves[entry['turnNumber']]
        analysis[index]['playedMove'] = "%s" % common.format_vertex(this_move[1])
        analysis[index]['playedColor'] = this_move[0]
        if this_move[0] == None:
            analysis[index]['playerName'] = None
            analysis[index]['opponentName'] = None

            analysis[index]['playerRank'] = None
            analysis[index]['opponentRank'] = None
            analysis[index]['location'] = None
            analysis[index]['result'] = game.get_root().get('RE')
        else:
            analysis[index]['playerName'] = player_names[this_move[0]]
            analysis[index]['opponentName'] = player_names['w' if this_move[0]=='b' else 'b']

            root_node = game.get_root()

            analysis[index]['result'] = root_node.get('RE')

            if this_move[0]=='b':
                if root_node.has_property("BR"):
                    analysis[index]['playerRank'] = utf8_string(game, root_node.get("BR"))
                if root_node.has_property("WR"):
                    analysis[index]['opponentRank'] = utf8_string(game, root_node.get("WR"))
            else:
                if root_node.has_property("BR"):
                    analysis[index]['opponentRank'] = utf8_string(game, root_node.get("BR"))
                if root_node.has_property("WR"):
                    analysis[index]['playerRank'] = utf8_string(game, root_node.get("WR"))

            if root_node.has_property("AP"):
                #analysis[index]['application'] = game, root_node.get_raw_list("AP")
                print(root_node.get_raw_list("AP"))

            if root_node.has_property("PC"):
                print("Location is %s" % root_node.get("PC"))
                analysis[index]['location'] = utf8_string(game, root_node.get("PC"))
            else:
                print("No location")


    sgf = os.path.basename(filepath)
    analysis[0]['winrate_change'] = 0
    for i in range(get_game_length(analysis, sgf)):
        current_index = get_move_index(analysis, sgf, i)

        # Needs to check for right SGF too
        if not analysis[current_index]['sgf'] == os.path.basename(filepath):
            # Not our SGF, skip this entry
            continue

        print ("Analysis %d/%s: %s" % (i, sgf, analysis[current_index]))

        if i == 0:
            analysis[current_index]['winrate_change'] = None
            analysis[current_index]['decolored_winrate_change'] = None
            continue

        current = get_move(analysis, sgf, i)
        prev = get_move(analysis, sgf, i-1)
        next = get_move(analysis, sgf, i+1)
        if prev == None:
            continue

        analysis[current_index]['winrate_change'] = current['winrate'] - prev['winrate']
        analysis[current_index]['decolored_winrate_change'] = analysis[current_index]['winrate_change'] if analysis[current_index]['playedColor'] == 'b' else analysis[current_index]['winrate_change'] * -1

    for i in range(get_game_length(analysis, sgf)-1, 0, -1):
        current_index = get_move_index(analysis, sgf, i)

        # Needs to check for right SGF too
        if not analysis[current_index]['sgf'] == os.path.basename(filepath):
            # Not our SGF, skip this entry
            continue
        current = get_move(analysis, sgf, i)
        prev = get_move(analysis, sgf, i-1)
        if prev == None:
            continue

        analysis[current_index]['bestKataMove'] = prev['bestKataMove']

    print (json.dumps(analysis))
    return analysis



def patch_names(analysis, filepath):
    # This will be _slow_ computationally - O(N^2). Work on more efficient in v2
    game = load_game(file)

    for index, value in enumerate(analysis):
        if ascii_hash(os.path.basename(filepath)) == value['sgf']:
            print("Updating names!")
            analysis[index]['sgf'] = os.path.basename(filepath)

    return analysis

#hardcoded_analysis = [{'winrate': 0.47953954096050855, 'turnNumber': 0, 'bestKataMove': 'Q16', 'playedMove': 'Q4', 'playedColor': 'b', 'winrate_change': 0}, {'winrate': 0.4859386198566459, 'turnNumber': 1, 'bestKataMove': 'D16', 'playedMove': 'D16', 'playedColor': 'w', 'winrate_change': 0.0063990788961373335}, {'winrate': 0.48512854408184913, 'turnNumber': 2, 'bestKataMove': 'C17', 'playedMove': 'D4', 'playedColor': 'b', 'winrate_change': -0.000810075774796748}, {'winrate': 0.48880407554470917, 'turnNumber': 3, 'bestKataMove': 'Q16', 'playedMove': 'Q16', 'playedColor': 'w', 'winrate_change': 0.0036755314628600377}, {'winrate': 0.4888511468378016, 'turnNumber': 4, 'bestKataMove': 'C17', 'playedMove': 'Q7', 'playedColor': 'b', 'winrate_change': 4.707129309244884e-05}, {'winrate': 0.44770251856364274, 'turnNumber': 5, 'bestKataMove': 'C3', 'playedMove': 'F3', 'playedColor': 'w', 'winrate_change': -0.04114862827415888}, {'winrate': 0.46825741501896245, 'turnNumber': 6, 'bestKataMove': 'H3', 'playedMove': 'E3', 'playedColor': 'b', 'winrate_change': 0.020554896455319716}, {'winrate': 0.4518902493343746, 'turnNumber': 7, 'bestKataMove': 'F4', 'playedMove': 'F4', 'playedColor': 'w', 'winrate_change': -0.016367165684587848}, {'winrate': 0.4517641436598646, 'turnNumber': 8, 'bestKataMove': 'C6', 'playedMove': 'C6', 'playedColor': 'b', 'winrate_change': -0.00012610567450999088}, {'winrate': 0.4575147715380071, 'turnNumber': 9, 'bestKataMove': 'Q3', 'playedMove': 'K3', 'playedColor': 'w', 'winrate_change': 0.005750627878142511}, {'winrate': 0.4691652760519882, 'turnNumber': 10, 'bestKataMove': 'C17', 'playedMove': 'R13', 'playedColor': 'b', 'winrate_change': 0.011650504513981086}, {'winrate': 0.4359647243532938, 'turnNumber': 11, 'bestKataMove': 'R15', 'playedMove': 'O17', 'playedColor': 'w', 'winrate_change': -0.0332005516986944}, {'winrate': 0.43988276785883795, 'turnNumber': 12, 'bestKataMove': 'C17', 'playedMove': 'D13', 'playedColor': 'b', 'winrate_change': 0.003918043505544144}, {'winrate': 0.38401399095642463, 'turnNumber': 13, 'bestKataMove': 'F17', 'playedMove': 'C14', 'playedColor': 'w', 'winrate_change': -0.05586877690241332}, {'winrate': 0.4137723112589351, 'turnNumber': 14, 'bestKataMove': 'C13', 'playedMove': 'C13', 'playedColor': 'b', 'winrate_change': 0.029758320302510466}, {'winrate': 0.4196668055079251, 'turnNumber': 15, 'bestKataMove': 'D14', 'playedMove': 'F16', 'playedColor': 'w', 'winrate_change': 0.005894494248990023}, {'winrate': 0.4876893304750509, 'turnNumber': 16, 'bestKataMove': 'B14', 'playedMove': 'D10', 'playedColor': 'b', 'winrate_change': 0.0680225249671258}, {'winrate': 0.3953570846241067, 'turnNumber': 17, 'bestKataMove': 'O3', 'playedMove': 'O3', 'playedColor': 'w', 'winrate_change': -0.09233224585094424}, {'winrate': 0.3869237486761674, 'turnNumber': 18, 'bestKataMove': 'S16', 'playedMove': 'R16', 'playedColor': 'b', 'winrate_change': -0.008433335947939269}, {'winrate': 0.33390046748554214, 'turnNumber': 19, 'bestKataMove': 'R15', 'playedMove': 'R17', 'playedColor': 'w', 'winrate_change': -0.05302328119062527}, {'winrate': 0.4167116618172304, 'turnNumber': 20, 'bestKataMove': 'R15', 'playedMove': 'S16', 'playedColor': 'b', 'winrate_change': 0.08281119433168826}, {'winrate': 0.32925923483870867, 'turnNumber': 21, 'bestKataMove': 'Q15', 'playedMove': 'Q15', 'playedColor': 'w', 'winrate_change': -0.08745242697852174}, {'winrate': 0.32623583750735186, 'turnNumber': 22, 'bestKataMove': 'S17', 'playedMove': 'S17', 'playedColor': 'b', 'winrate_change': -0.003023397331356814}, {'winrate': 0.3286116141726608, 'turnNumber': 23, 'bestKataMove': 'R18', 'playedMove': 'R18', 'playedColor': 'w', 'winrate_change': 0.0023757766653089707}, {'winrate': 0.32260177936992096, 'turnNumber': 24, 'bestKataMove': 'N17', 'playedMove': 'P3', 'playedColor': 'b', 'winrate_change': -0.006009834802739866}, {'winrate': 0.28253624815200884, 'turnNumber': 25, 'bestKataMove': 'O4', 'playedMove': 'O4', 'playedColor': 'w', 'winrate_change': -0.04006553121791212}, {'winrate': 0.2803883120969297, 'turnNumber': 26, 'bestKataMove': 'M3', 'playedMove': 'R10', 'playedColor': 'b', 'winrate_change': -0.002147936055079125}, {'winrate': 0.2266587662469981, 'turnNumber': 27, 'bestKataMove': 'Q11', 'playedMove': 'J16', 'playedColor': 'w', 'winrate_change': -0.053729545849931615}, {'winrate': 0.2684703246767076, 'turnNumber': 28, 'bestKataMove': 'N17', 'playedMove': 'L15', 'playedColor': 'b', 'winrate_change': 0.04181155842970952}, {'winrate': 0.18054661979773134, 'turnNumber': 29, 'bestKataMove': 'Q11', 'playedMove': 'L17', 'playedColor': 'w', 'winrate_change': -0.08792370487897627}, {'winrate': 0.22279194426319293, 'turnNumber': 30, 'bestKataMove': 'M3', 'playedMove': 'M5', 'playedColor': 'b', 'winrate_change': 0.04224532446546159}, {'winrate': 0.17782803898281052, 'turnNumber': 31, 'bestKataMove': 'R3', 'playedMove': 'N6', 'playedColor': 'w', 'winrate_change': -0.04496390528038241}, {'winrate': 0.28503899629392493, 'turnNumber': 32, 'bestKataMove': 'M3', 'playedMove': 'N9', 'playedColor': 'b', 'winrate_change': 0.10721095731111441}, {'winrate': 0.16569659149131444, 'turnNumber': 33, 'bestKataMove': 'M3', 'playedMove': 'F7', 'playedColor': 'w', 'winrate_change': -0.11934240480261049}, {'winrate': 0.25368160817293495, 'turnNumber': 34, 'bestKataMove': 'M3', 'playedMove': 'J7', 'playedColor': 'b', 'winrate_change': 0.08798501668162051}, {'winrate': 0.13479863396275593, 'turnNumber': 35, 'bestKataMove': 'R3', 'playedMove': 'K6', 'playedColor': 'w', 'winrate_change': -0.11888297421017902}, {'winrate': 0.17883916764022523, 'turnNumber': 36, 'bestKataMove': 'M3', 'playedMove': 'K7', 'playedColor': 'b', 'winrate_change': 0.044040533677469296}, {'winrate': 0.1537951593768414, 'turnNumber': 37, 'bestKataMove': 'M6', 'playedMove': 'L6', 'playedColor': 'w', 'winrate_change': -0.02504400826338382}, {'winrate': 0.19233875181392546, 'turnNumber': 38, 'bestKataMove': 'M6', 'playedMove': 'L7', 'playedColor': 'b', 'winrate_change': 0.03854359243708405}, {'winrate': 0.06249448544635183, 'turnNumber': 39, 'bestKataMove': 'M6', 'playedMove': 'M6', 'playedColor': 'w', 'winrate_change': -0.12984426636757362}, {'winrate': 0.06136295486344534, 'turnNumber': 40, 'bestKataMove': 'O2', 'playedMove': 'M8', 'playedColor': 'b', 'winrate_change': -0.0011315305829064926}, {'winrate': 0.022155022638584265, 'turnNumber': 41, 'bestKataMove': 'R3', 'playedMove': 'C8', 'playedColor': 'w', 'winrate_change': -0.039207932224861075}, {'winrate': 0.036632225039940236, 'turnNumber': 42, 'bestKataMove': 'E6', 'playedMove': 'O14', 'playedColor': 'b', 'winrate_change': 0.01447720240135597}, {'winrate': 0.018843797520564642, 'turnNumber': 43, 'bestKataMove': 'R3', 'playedMove': 'Q13', 'playedColor': 'w', 'winrate_change': -0.017788427519375594}, {'winrate': 0.02808276842542079, 'turnNumber': 44, 'bestKataMove': 'R14', 'playedMove': 'Q12', 'playedColor': 'b', 'winrate_change': 0.009238970904856147}, {'winrate': 0.010694832862657933, 'turnNumber': 45, 'bestKataMove': 'R14', 'playedMove': 'P13', 'playedColor': 'w', 'winrate_change': -0.017387935562762857}, {'winrate': 0.023945323795197426, 'turnNumber': 46, 'bestKataMove': 'R14', 'playedMove': 'O12', 'playedColor': 'b', 'winrate_change': 0.013250490932539494}, {'winrate': 0.004490907704343572, 'turnNumber': 47, 'bestKataMove': 'R14', 'playedMove': 'P12', 'playedColor': 'w', 'winrate_change': -0.019454416090853854}, {'winrate': 0.009454739412677493, 'turnNumber': 48, 'bestKataMove': 'R14', 'playedMove': 'P11', 'playedColor': 'b', 'winrate_change': 0.004963831708333921}, {'winrate': 0.003737318573679871, 'turnNumber': 49, 'bestKataMove': 'R14', 'playedMove': 'Q11', 'playedColor': 'w', 'winrate_change': -0.005717420838997622}, {'winrate': 0.012676314193846183, 'turnNumber': 50, 'bestKataMove': 'R12', 'playedMove': 'R12', 'playedColor': 'b', 'winrate_change': 0.008938995620166312}, {'winrate': 0.012795033394089717, 'turnNumber': 51, 'bestKataMove': 'O13', 'playedMove': 'O11', 'playedColor': 'w', 'winrate_change': 0.00011871920024353333}, {'winrate': 0.013354151782360102, 'turnNumber': 52, 'bestKataMove': 'P10', 'playedMove': 'P10', 'playedColor': 'b', 'winrate_change': 0.0005591183882703854}, {'winrate': 0.013494025985380853, 'turnNumber': 53, 'bestKataMove': 'O13', 'playedMove': 'O13', 'playedColor': 'w', 'winrate_change': 0.00013987420302075115}, {'winrate': 0.012388660616296732, 'turnNumber': 54, 'bestKataMove': 'O2', 'playedMove': 'N12', 'playedColor': 'b', 'winrate_change': -0.0011053653690841214}, {'winrate': 0.0072288737622929755, 'turnNumber': 55, 'bestKataMove': 'N13', 'playedMove': 'N13', 'playedColor': 'w', 'winrate_change': -0.005159786854003756}, {'winrate': 0.006899917497329988, 'turnNumber': 56, 'bestKataMove': 'O2', 'playedMove': 'M13', 'playedColor': 'b', 'winrate_change': -0.00032895626496298735}, {'winrate': 0.006221208626344454, 'turnNumber': 57, 'bestKataMove': 'M12', 'playedMove': 'M12', 'playedColor': 'w', 'winrate_change': -0.0006787088709855338}, {'winrate': 0.00610389404475753, 'turnNumber': 58, 'bestKataMove': 'N11', 'playedMove': 'N11', 'playedColor': 'b', 'winrate_change': -0.00011731458158692476}, {'winrate': 0.006348162884327935, 'turnNumber': 59, 'bestKataMove': 'M14', 'playedMove': 'M14', 'playedColor': 'w', 'winrate_change': 0.00024426883957040513}, {'winrate': 0.005626222584064977, 'turnNumber': 60, 'bestKataMove': 'O2', 'playedMove': 'L13', 'playedColor': 'b', 'winrate_change': -0.0007219403002629576}, {'winrate': 0.006150301264249869, 'turnNumber': 61, 'bestKataMove': 'R14', 'playedMove': 'M11', 'playedColor': 'w', 'winrate_change': 0.0005240786801848918}, {'winrate': 0.006064941666767409, 'turnNumber': 62, 'bestKataMove': 'N10', 'playedMove': 'N10', 'playedColor': 'b', 'winrate_change': -8.535959748245947e-05}, {'winrate': 0.006148670124562106, 'turnNumber': 63, 'bestKataMove': 'L14', 'playedMove': 'K13', 'playedColor': 'w', 'winrate_change': 8.372845779469618e-05}, {'winrate': 0.04226258814218764, 'turnNumber': 64, 'bestKataMove': 'L14', 'playedMove': 'L14', 'playedColor': 'b', 'winrate_change': 0.036113918017625535}, {'winrate': 0.043113461879803805, 'turnNumber': 65, 'bestKataMove': 'R3', 'playedMove': 'M15', 'playedColor': 'w', 'winrate_change': 0.0008508737376161646}, {'winrate': 0.060419798505209954, 'turnNumber': 66, 'bestKataMove': 'O2', 'playedMove': 'L12', 'playedColor': 'b', 'winrate_change': 0.01730633662540615}, {'winrate': 0.03558578243698696, 'turnNumber': 67, 'bestKataMove': 'R14', 'playedMove': 'B14', 'playedColor': 'w', 'winrate_change': -0.024834016068222997}, {'winrate': 0.04567217317648242, 'turnNumber': 68, 'bestKataMove': 'O2', 'playedMove': 'G13', 'playedColor': 'b', 'winrate_change': 0.010086390739495465}, {'winrate': 0.02682403181833104, 'turnNumber': 69, 'bestKataMove': 'R3', 'playedMove': 'J13', 'playedColor': 'w', 'winrate_change': -0.018848141358151382}, {'winrate': 0.1330975402122586, 'turnNumber': 70, 'bestKataMove': 'O2', 'playedMove': 'H12', 'playedColor': 'b', 'winrate_change': 0.10627350839392757}, {'winrate': 0.03736568104834381, 'turnNumber': 71, 'bestKataMove': 'R3', 'playedMove': 'B10', 'playedColor': 'w', 'winrate_change': -0.0957318591639148}, {'winrate': 0.09321981320140171, 'turnNumber': 72, 'bestKataMove': 'O2', 'playedMove': 'F8', 'playedColor': 'b', 'winrate_change': 0.0558541321530579}, {'winrate': 0.031636059769894365, 'turnNumber': 73, 'bestKataMove': 'R14', 'playedMove': 'G8', 'playedColor': 'w', 'winrate_change': -0.06158375343150735}, {'winrate': 0.06338210269612132, 'turnNumber': 74, 'bestKataMove': 'E7', 'playedMove': 'G9', 'playedColor': 'b', 'winrate_change': 0.03174604292622696}, {'winrate': 0.018493276050725704, 'turnNumber': 75, 'bestKataMove': 'R3', 'playedMove': 'G6', 'playedColor': 'w', 'winrate_change': -0.04488882664539562}, {'winrate': 0.05921716747314676, 'turnNumber': 76, 'bestKataMove': 'F2', 'playedMove': 'H8', 'playedColor': 'b', 'winrate_change': 0.040723891422421055}, {'winrate': 0.020126262257886607, 'turnNumber': 77, 'bestKataMove': 'R3', 'playedMove': 'J6', 'playedColor': 'w', 'winrate_change': -0.03909090521526015}, {'winrate': 0.17249835548332115, 'turnNumber': 78, 'bestKataMove': 'O2', 'playedMove': 'D8', 'playedColor': 'b', 'winrate_change': 0.15237209322543455}, {'winrate': 0.03344315528481867, 'turnNumber': 79, 'bestKataMove': 'R3', 'playedMove': 'B13', 'playedColor': 'w', 'winrate_change': -0.13905520019850248}, {'winrate': 0.11619150895774466, 'turnNumber': 80, 'bestKataMove': 'O2', 'playedMove': 'B12', 'playedColor': 'b', 'winrate_change': 0.08274835367292599}, {'winrate': 0.027697260671583734, 'turnNumber': 81, 'bestKataMove': 'R3', 'playedMove': 'C12', 'playedColor': 'w', 'winrate_change': -0.08849424828616093}, {'winrate': 0.10536045690944218, 'turnNumber': 82, 'bestKataMove': 'O2', 'playedMove': 'C11', 'playedColor': 'b', 'winrate_change': 0.07766319623785845}, {'winrate': 0.050537519659225705, 'turnNumber': 83, 'bestKataMove': 'R3', 'playedMove': 'B11', 'playedColor': 'w', 'winrate_change': -0.054822937250216475}, {'winrate': 0.16706250297578396, 'turnNumber': 84, 'bestKataMove': 'O2', 'playedMove': 'A12', 'playedColor': 'b', 'winrate_change': 0.11652498331655825}, {'winrate': 0.16453076374397324, 'turnNumber': 85, 'bestKataMove': 'E8', 'playedMove': 'D12', 'playedColor': 'w', 'winrate_change': -0.0025317392318107146}, {'winrate': 0.44163277618538044, 'turnNumber': 86, 'bestKataMove': 'E12', 'playedMove': 'E12', 'playedColor': 'b', 'winrate_change': 0.2771020124414072}, {'winrate': 0.4404962991966015, 'turnNumber': 87, 'bestKataMove': 'R14', 'playedMove': 'C10', 'playedColor': 'w', 'winrate_change': -0.001136476988778945}, {'winrate': 0.9153149835233481, 'turnNumber': 88, 'bestKataMove': 'D11', 'playedMove': 'D11', 'playedColor': 'b', 'winrate_change': 0.47481868432674657}, {'winrate': 0.9152377966063756, 'turnNumber': 89, 'bestKataMove': 'E8', 'playedMove': 'C7', 'playedColor': 'w', 'winrate_change': -7.718691697244928e-05}, {'winrate': 0.9280263503546231, 'turnNumber': 90, 'bestKataMove': 'D6', 'playedMove': 'D7', 'playedColor': 'b', 'winrate_change': 0.0127885537482475}, {'winrate': 0.8465376327865513, 'turnNumber': 91, 'bestKataMove': 'B6', 'playedMove': 'B6', 'playedColor': 'w', 'winrate_change': -0.08148871756807186}, {'winrate': 0.8429917507961395, 'turnNumber': 92, 'bestKataMove': 'C5', 'playedMove': 'A11', 'playedColor': 'b', 'winrate_change': -0.0035458819904117256}, {'winrate': 0.04914907067718066, 'turnNumber': 93, 'bestKataMove': 'C5', 'playedMove': 'C5', 'playedColor': 'w', 'winrate_change': -0.7938426801189589}, {'winrate': 0.04801824477113814, 'turnNumber': 94, 'bestKataMove': 'B4', 'playedMove': 'A10', 'playedColor': 'b', 'winrate_change': -0.0011308259060425208}, {'winrate': 0.006988465374641706, 'turnNumber': 95, 'bestKataMove': 'B4', 'playedMove': 'D5', 'playedColor': 'w', 'winrate_change': -0.041029779396496435}, {'winrate': 0.00741187427731127, 'turnNumber': 96, 'bestKataMove': 'B4', 'playedMove': 'C9', 'playedColor': 'b', 'winrate_change': 0.0004234089026695642}, {'winrate': 0.0020600402180903643, 'turnNumber': 97, 'bestKataMove': 'R14', 'playedMove': 'B9', 'playedColor': 'w', 'winrate_change': -0.005351834059220906}, {'winrate': 0.005198354200059985, 'turnNumber': 98, 'bestKataMove': 'B4', 'playedMove': 'B8', 'playedColor': 'b', 'winrate_change': 0.0031383139819696204}, {'winrate': 0.0031924225097376135, 'turnNumber': 99, 'bestKataMove': 'D6', 'playedMove': 'D9', 'playedColor': 'w', 'winrate_change': -0.002005931690322371}, {'winrate': 0.003143570164461762, 'turnNumber': 100, 'bestKataMove': 'B4', 'playedMove': 'A9', 'playedColor': 'b', 'winrate_change': -4.885234527585158e-05}, {'winrate': 0.0020226506193095073, 'turnNumber': 101, 'bestKataMove': 'R3', 'playedMove': 'C9', 'playedColor': 'w', 'winrate_change': -0.0011209195451522547}, {'winrate': 0.003654629055329539, 'turnNumber': 102, 'bestKataMove': 'B4', 'playedMove': 'B7', 'playedColor': 'b', 'winrate_change': 0.0016319784360200318}, {'winrate': 0.0026608966628560715, 'turnNumber': 103, 'bestKataMove': 'D6', 'playedMove': 'D6', 'playedColor': 'w', 'winrate_change': -0.0009937323924734676}, {'winrate': 0.002234190597962371, 'turnNumber': 104, 'bestKataMove': 'B3', 'playedMove': 'E9', 'playedColor': 'b', 'winrate_change': -0.00042670606489370044}, {'winrate': 0.0022201423327208314, 'turnNumber': 105, 'bestKataMove': 'R14', 'playedMove': 'C6', 'playedColor': 'w', 'winrate_change': -1.4048265241539681e-05}, {'winrate': 0.002839692226023982, 'turnNumber': 106, 'bestKataMove': 'B3', 'playedMove': 'C4', 'playedColor': 'b', 'winrate_change': 0.0006195498933031507}, {'winrate': 0.0027463774301589305, 'turnNumber': 107, 'bestKataMove': 'R3', 'playedMove': 'B3', 'playedColor': 'w', 'winrate_change': -9.331479586505154e-05}, {'winrate': 0.005480713912441981, 'turnNumber': 108, 'bestKataMove': 'B4', 'playedMove': 'B4', 'playedColor': 'b', 'winrate_change': 0.0027343364822830507}, {'winrate': 0.005606512637925665, 'turnNumber': 109, 'bestKataMove': 'R3', 'playedMove': 'B2', 'playedColor': 'w', 'winrate_change': 0.00012579872548368343}, {'winrate': 0.008893261979743827, 'turnNumber': 110, 'bestKataMove': 'A4', 'playedMove': 'C3', 'playedColor': 'b', 'winrate_change': 0.0032867493418181626}, {'winrate': 0.003319500823263155, 'turnNumber': 111, 'bestKataMove': 'D2', 'playedMove': 'D2', 'playedColor': 'w', 'winrate_change': -0.005573761156480672}, {'winrate': 0.003447823219608237, 'turnNumber': 112, 'bestKataMove': 'E2', 'playedMove': 'C2', 'playedColor': 'b', 'winrate_change': 0.0001283223963450819}, {'winrate': 0.0011591480241530672, 'turnNumber': 113, 'bestKataMove': 'E2', 'playedMove': 'E2', 'playedColor': 'w', 'winrate_change': -0.00228867519545517}, {'winrate': 0.0009588036037309067, 'turnNumber': 114, 'bestKataMove': 'B1', 'playedMove': 'B1', 'playedColor': 'b', 'winrate_change': -0.0002003444204221605}, {'winrate': 0.0010213824906216695, 'turnNumber': 115, 'bestKataMove': 'A2', 'playedMove': 'A2', 'playedColor': 'w', 'winrate_change': 6.257888689076285e-05}, {'winrate': 0.0009949225454906063, 'turnNumber': 116, 'bestKataMove': 'O2', 'playedMove': 'C17', 'playedColor': 'b', 'winrate_change': -2.64599451310632e-05}, {'winrate': 0.0010438921082753794, 'turnNumber': 117, 'bestKataMove': 'D18', 'playedMove': 'D17', 'playedColor': 'w', 'winrate_change': 4.8969562784773046e-05}, {'winrate': 0.0010303639303987921, 'turnNumber': 118, 'bestKataMove': 'O2', 'playedMove': 'C16', 'playedColor': 'b', 'winrate_change': -1.3528177876587222e-05}, {'winrate': 0.0009929513413966529, 'turnNumber': 119, 'bestKataMove': 'D15', 'playedMove': 'D15', 'playedColor': 'w', 'winrate_change': -3.7412589002139285e-05}, {'winrate': 0.0009570909073617351, 'turnNumber': 120, 'bestKataMove': 'O2', 'playedMove': 'C15', 'playedColor': 'b', 'winrate_change': -3.586043403491779e-05}, {'winrate': 0.0009150999440190599, 'turnNumber': 121, 'bestKataMove': 'D14', 'playedMove': 'D14', 'playedColor': 'w', 'winrate_change': -4.199096334267516e-05}, {'winrate': 0.0008360596736829429, 'turnNumber': 122, 'bestKataMove': 'O2', 'playedMove': 'B15', 'playedColor': 'b', 'winrate_change': -7.904027033611705e-05}, {'winrate': 0.0006284416602381215, 'turnNumber': 123, 'bestKataMove': 'C18', 'playedMove': 'C18', 'playedColor': 'w', 'winrate_change': -0.00020761801344482134}, {'winrate': 0.0006108130062405515, 'turnNumber': 124, 'bestKataMove': 'O2', 'playedMove': 'B18', 'playedColor': 'b', 'winrate_change': -1.7628653997570076e-05}, {'winrate': 0.0005433917148895961, 'turnNumber': 125, 'bestKataMove': 'D18', 'playedMove': 'D18', 'playedColor': 'w', 'winrate_change': -6.742129135095531e-05}, {'winrate': 0.0005119373020978646, 'turnNumber': 126, 'bestKataMove': 'O2', 'playedMove': 'B19', 'playedColor': 'b', 'winrate_change': -3.1454412791731556e-05}, {'winrate': 0.0004462409769833142, 'turnNumber': 127, 'bestKataMove': 'A16', 'playedMove': 'A16', 'playedColor': 'w', 'winrate_change': -6.569632511455037e-05}]

kata = None

files = os.listdir(SGF_PATH)

moves_to_collect = 0
for file in files:
    game = load_game(file)

    if game == None:
        continue

    if kata == None:
        kata = start_kata_go()
    new_moves = process_sgf(kata, file, game, gamename=os.path.basename(file))
    print('[*] Sent %d messages from %s SGF' % (new_moves, os.path.basename(file)))
    moves_to_collect = moves_to_collect + new_moves


print('[*] All messages sent to kata.')
print('[*] Expecting %d responses.' % moves_to_collect)
analysis = collect_kata_responses(kata, moves_to_collect)
print(analysis)
print('[*] All messages collected from kata.')

for file in files:
    analysis = patch_names(analysis, file)
    analysis = annotate_analysis(analysis, file)

analysis = serialize_analysis(analysis)
